﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BookClass
{
    /// <summary>BookComparator</summary>
    public class BookPriceComparator : IComparer<Book>
    {
        /// <summary>Compare</summary>
        /// <returns>int</returns>
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook.Price.CompareTo(secondBook.Price);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BookClass
{
    /// <summary>BookComparator</summary>
    public class BookPagesComparator : IComparer<Book>
    {
        /// <summary>Compare</summary>
        /// <returns>int</returns>
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook.Pages.CompareTo(secondBook.Pages);
        }
    }

}

﻿using System;
using NUnit.Framework;

#pragma warning disable CA1707 // Identifiers should not contain underscores
#pragma warning disable SA1600 // Elements should be documented

namespace BookClass.Tests
{
    [TestFixture]
    public class BookTests
    {
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void Book_CreateNewThreeParameters(string author, string title, string publisher)
        {
            Book book = new Book(author, title, publisher); 
            Assert.IsTrue(book.Author == author && book.Title == title && book.Publisher == publisher); 
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532")]
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", "1617294532")]
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", "")]
        public void Book_CreateNewFourParameters(string author, string title, string publisher, string isbn)
        {
            Book book = new Book(author, title, publisher, isbn);
            Assert.IsTrue(book.Author == author && book.Title == title && book.Publisher == publisher && book.ISBN == isbn);
        }

        [TestCase(null, "C# in Depth", "Manning Publications")]
        [TestCase("Jon Skeet", null, "Manning Publications")]
        [TestCase("Jon Skeet", "C# in Depth", null)]
        public void Book_CreateNewThreeParameters_ThrowArgumentNullException(string author, string title, string publisher)
        {
            Assert.Throws<ArgumentNullException>(() => new Book(author, title, publisher), "author or title or publisher cannot be null");
        }

        [TestCase(null, "C# in Depth", "Manning Publications", "978-0-901-55580-4")]
        [TestCase("Jon Skeet", null, "Manning Publications", "978-0-901-55580-4")]
        [TestCase("Jon Skeet", "C# in Depth", null, "978-0-901-55580-4")]
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", null)]
        public void Book_CreateNewFourParameters_ThrowArgumentNullException(string author, string title, string publisher, string isbn)
        {
            Assert.Throws<ArgumentNullException>(() => new Book(author, title, publisher, isbn), "author or title or publisher or ISBN cannot be null");
        }

        [Test]
        public void Book_PagesTest()
        {
            int expected = 528;
            Book book = new Book(string.Empty, string.Empty, string.Empty)
            {
                Pages = expected,
            };
            Assert.AreEqual(expected, book.Pages);
        }

        [TestCase(-1)]
        [TestCase(0)]
        public void Book_PagesTest_ArgumentOutOfRangeException(int pages)
        {
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            Assert.Throws<ArgumentOutOfRangeException>(() => book.Pages = pages, "Count of pages should be greater than zero.");
        }

        [Test]
        public void Book_Publish_GetPublicationDate_Tests()
        {
            DateTime expected = DateTime.Now;
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            book.Publish(expected);

            Assert.AreEqual(FormattableString.Invariant($"{expected:d}"), book.GetPublicationDate());
        }

        [Test]
        public void Book_Publish_GetPublicationDate_Empty_Tests()
        {
            string expected = "NYP";
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            Assert.AreEqual(expected, book.GetPublicationDate());
        }

        [Test]
        public void Book_SetPrice_Tests()
        {
            decimal price = 50;
            string currency = "USD";
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            book.SetPrice(price, currency);
            Assert.IsTrue(book.Price == price && book.Currency == currency);
        }

        [Test]
        public void Book_SetPrice_InvalidCurrency_ArgumentException()
        {
            decimal price = 50;
            string currency = "a";
            Book book = new Book(string.Empty, string.Empty, string.Empty);

            Assert.Throws<ArgumentException>(() => book.SetPrice(price, currency), "Currency is invalid.");
        }

        [Test]
        public void Book_SetPrice_LessZero_ArgumentException()
        {
            decimal price = -50;
            string currency = "USD";
            Book book = new Book(string.Empty, string.Empty, string.Empty);

            Assert.Throws<ArgumentException>(() => book.SetPrice(price, currency), "Price cannot be less than zero.");
        }

        [Test]
        public void Book_SetPrice_CurrencyIsNull_ArgumentNullException()
        {
            decimal price = 50;
            Book book = new Book(string.Empty, string.Empty, string.Empty);

            Assert.Throws<ArgumentNullException>(() => book.SetPrice(price, null), "Currency cannot be null.");
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", ExpectedResult = "C# in Depth by Jon Skeet")]
        [TestCase("Jon Skeet", "", "Manning Publications", ExpectedResult = " by Jon Skeet")]
        [TestCase("", "C# in Depth", "Manning Publications", ExpectedResult = "C# in Depth by ")]
        public string Book_ToString(string author, string title, string publisher)
            => new Book(author, title, publisher).ToString();

        [Test]
        public void Book_Publish_NotPublished_Test()
        {
            Book book = new Book("Author", "Title", "Publisher");
            Assert.AreEqual(book.GetPublicationDate(), "NYP");
        }

        [Test]
        public void Book_Publish_Test()
        {
            const int year = 2021;
            const int month = 10;
            const int day = 12;
            DateTime date = new DateTime(year, month, day);

            Book book = new Book("Author", "Title", "Publisher");
            book.Publish(date);
            Assert.AreEqual(book.GetPublicationDate(), $"{month}/{day}/{year}");
        }        

        [TestCase("Hermann Hesse ", "The Glass Bead Game", "Picador", true, ExpectedResult = -450244879)]
        [TestCase("Hermann Hesse", "Demian", "Samaira Book Publishers", false, ExpectedResult = -1405177133)]
        public int Book_GetHashCode(string author, string title, string publisher, bool published)
        {
            Book book = new Book(author, title, publisher);
            if (published)
            {
                book.Publish(DateTime.Now);
            }

            return book.GetHashCode();
        }

        [TestCase("9783518188163", "978-1501142970", ExpectedResult = true)]
        [TestCase("978-1501142970", "8-555-12697-3", ExpectedResult = false)]
        public bool Book_Equals(string ISBN1, string ISBN2) => new Book(string.Empty, string.Empty, string.Empty, ISBN1).Equals(new Book(string.Empty, string.Empty, string.Empty, ISBN2));

        [TestCase("Demian", "Demian", ExpectedResult = 0)]
        [TestCase("The Glass Bead Game", "Demian", ExpectedResult = 1)]
        [TestCase("Demian", "The Glass Bead Game", ExpectedResult = -1)]
        public int Book_CompareTo_Title(string Title1, string Title2) => 
                                        new Book(string.Empty, Title1, string.Empty)
                                       .CompareTo(new Book(string.Empty, Title2, string.Empty));


        [TestCase(100, 100,  ExpectedResult = 0)]
        [TestCase( 101, 100, ExpectedResult = 1)]
        [TestCase(99, 100, ExpectedResult = -1)]
        public int Book_Pages_Comparison_Test(int pages1, int pages2)
        {
            Book book1 = new Book("Hermann Hesse", "Demian", "Samaira Book Publishers");
            Book book2 = new Book("Hermann Hesse", "Demian", "Samaira Book Publishers");
            book1.Pages = pages1;
            book2.Pages = pages2;
            return new BookPagesComparator().Compare(book1, book2);
        }
        
        [TestCase(50, 50,  ExpectedResult = 0)]
        [TestCase( 49, 50, ExpectedResult = -1)]
        [TestCase(51, 50, ExpectedResult = 1)]
        public int Book_Price_Comparison_Test(int price1, int price2)
        {
            Book book1 = new Book("Hermann Hesse", "Demian", "Samaira Book Publishers");
            Book book2 = new Book("Hermann Hesse", "Demian", "Samaira Book Publishers");
            book1.SetPrice(price1, "USD"); 
            book2.SetPrice(price2, "USD"); 
            return new BookPriceComparator().Compare(book1, book2);
        }

        [TestCase("Hermann Hesse", "Hermann Hesse", ExpectedResult = 0)]
        [TestCase("Jon Skeet", "Hermann Hesse", ExpectedResult = 2)]
        [TestCase("Hermann Hesse", "Jon Skeet", ExpectedResult = -2)]
        public int Book_Author_Comparison_Test(string author1, string author2)
        {
            Book book1 = new Book(author1, "Demian", "Samaira Book Publishers");
            Book book2 = new Book(author2, "C# in Depth", "Manning Publications");
            return new BookAuthorComparator().Compare(book1, book2);
        }
    }
}
